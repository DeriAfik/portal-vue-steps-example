import FirstStep from './components/FirstStep.vue';
import SecondStep from './components/SecondStep.vue';
import ThirdStep from './components/ThirdStep.vue';

export const routes = [
    {
        path: '/first',
        component: FirstStep
    },
    {
        path: '/second',
        component: SecondStep
    },
    {
        path: '/third',
        component: ThirdStep
    }
];