## Portal Vue with Vue Router example.

#### Installation
```js
// Clone the repo
git clone https://bitbucket.org/DeriAfik/portal-vue-steps-example

// install dependencies
npm install

// run the dev environment
npm run serve
```

#### Issue description
I have here a steps form with 3 different steps.
the Vue Router is responsible for switching the current step in the form based on the route.

In all steps I only have `BACK` and `NEXT` buttons at the bottom but on the second step
I have a portal that adds an extra button to this step alone.

The problem is that once I hit the second step, portal-vue then renders the extra button, but when I leave the second step the button is still there.

I'm guessing that since I have a `<keep-alive>` component wrapping my `<router-view>` it's not getting dismounted from the portal.

Please let me know what you guys think and if I'm doing something wrong.